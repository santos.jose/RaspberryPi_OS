# /bin/bash
# info https:/raspberryparanovatos.com/tutoriales/compartir-carpeta-samba-raspberry-pi-windows/
sudo apt update && sudo apt install samba samba-common -y
sudo cat smb.conf >> /etc/samba/smb.conf
#crear contraseña para el usuario pi
sudo smbpasswd -a pi
sudo service smbd restart