# /usr/bin
# info https://ivancarosati.com/no-ip-with-raspberry-pi/
cd /usr/local/src
sudo wget https://www.noip.com/client/linux/noip-duc-linux.tar.gz
sudo tar xzf noip-duc-linux.tar.gz
sudo rm noip-duc-linux.tar.gz
cd noip-*
sudo make
sudo make install
#complete information
sudo cp noip2.service /etc/systemd/system/noip2.service
sudo systemctl start noip2
sudo systemctl enable noip2